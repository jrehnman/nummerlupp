package com.endlesspipe.nummerlupp;

/**
 * Created by rehnman on 6/7/13.
 */
public interface SearchResult {
    public String getName();
}
