package com.endlesspipe.nummerlupp;


import android.net.http.AndroidHttpClient;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created by rehnman on 6/7/13.
 */
public class QueryClient implements Runnable {
    private String phoneNumber;
    private NumberQuery parent;


    public QueryClient(NumberQuery nq, String number) {
        phoneNumber = number;
        parent = nq;
    }

    @Override
    public void run() {
        try {
            String sanitizedNumber = phoneNumber; //phoneNumber.replaceAll("^[0-9]", "");
            String queryString = String.format(
                    "http://developer.118100.se:8080/openapi-1.1/appetizing?query=%s&key=%s",
                    sanitizedNumber,
                    LicenseKey.KEY);

            AndroidHttpClient httpClient = AndroidHttpClient.newInstance("Foo");
            HttpGet request = new HttpGet(queryString);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream contentStream = entity.getContent();
                parseResponse(contentStream);
                entity.consumeContent();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void parseResponse(InputStream contentStream) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(contentStream);
            XPathFactory xPathFactory = XPathFactory.newInstance();
            XPath xpath = xPathFactory.newXPath();

            NodeList results = (NodeList) xpath.evaluate("//personHits/person[@sequenceNumber=0] | //companyHits/company[@sequenceNumber=0]", doc, XPathConstants.NODESET);
            if (results != null && results.getLength() > 0) {
                Node firstResult = results.item(0);
                String nodeName = firstResult.getNodeName();
                if (nodeName.equals("person")) {
                    PersonResult person = new PersonResult(firstResult);
                    parent.personResult(phoneNumber, person);
                    return;
                }
                else if (nodeName.equals("company")) {
                    CompanyResult company = new CompanyResult(firstResult);
                    parent.companyResult(phoneNumber, company);
                    return;
                }
            }
            parent.noResult(phoneNumber);
            return;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        parent.searchError(phoneNumber);
    }
}
