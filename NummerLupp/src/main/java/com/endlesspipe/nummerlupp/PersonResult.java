package com.endlesspipe.nummerlupp;

import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created by rehnman on 6/7/13.
 */
public class PersonResult implements SearchResult {
    XPathFactory xPathFactory = XPathFactory.newInstance();
    XPath xpath = xPathFactory.newXPath();
    String name;

    public PersonResult(Node personNode) {
        try {
            name = (String) xpath.evaluate("concat(//name[@type='first'], ' ',//name[@type='last'])", personNode, XPathConstants.STRING);
        } catch (XPathExpressionException e) {
            name = "Unknown (Company)";
        }
    }

    @Override
    public String getName() {
        return name;
    }
}
