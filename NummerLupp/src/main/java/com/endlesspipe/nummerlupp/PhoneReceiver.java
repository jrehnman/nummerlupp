package com.endlesspipe.nummerlupp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * Created by rehnman on 6/7/13.
 */
public class PhoneReceiver extends BroadcastReceiver {
    public static final String ACTION_PHONE_NUMBER_SEARCH = "com.endlesspipe.intent.action.PHONE_NUMBER_SEARCH";

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        String callState = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        if (action != null &&
                action.equals(TelephonyManager.ACTION_PHONE_STATE_CHANGED) &&
                callState != null &&
                callState.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
            String phoneNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

            if (phoneNumber != null) {
                Intent i = new Intent();
                i.setAction(ACTION_PHONE_NUMBER_SEARCH);
                i.putExtra(Intent.EXTRA_PHONE_NUMBER, phoneNumber);

                context.startService(i);
            }
        }
    }
}
