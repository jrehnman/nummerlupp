package com.endlesspipe.nummerlupp;

import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created by rehnman on 6/7/13.
 */
public class CompanyResult implements SearchResult {
    XPathFactory xPathFactory = XPathFactory.newInstance();
    XPath xpath = xPathFactory.newXPath();
    String name;

    public CompanyResult(Node companyNode) {
        try {
            name = (String) xpath.evaluate("//name[@type='legal']", companyNode, XPathConstants.STRING);
        } catch (XPathExpressionException e) {
            name = "Unknown (Company)";
        }
    }

    @Override
    public String getName() {
        return name;
    }
}
