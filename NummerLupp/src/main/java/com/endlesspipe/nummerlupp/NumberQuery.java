package com.endlesspipe.nummerlupp;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract.PhoneLookup;

/**
 * Created by rehnman on 6/7/13.
 */
public class NumberQuery extends IntentService {
    private NotificationManager notificationManager = null;

    public NumberQuery() {
        super("NumberQuery");
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        if (action != null && action.equals(PhoneReceiver.ACTION_PHONE_NUMBER_SEARCH)) {
            String phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
            if (phoneNumber != null && !contactExists(phoneNumber)) {
                lookupNumber(phoneNumber);
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        }
    }

    public void lookupNumber(String phoneNumber) {
        Handler handler = new Handler();
        handler.post(new QueryClient(this, phoneNumber));

        Notification.Builder b = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(phoneNumber)
                .setContentText("Searching for number")
                .setTicker(String.format("Searching: %s", phoneNumber));
        notificationManager.notify(0, b.build());
    }

    public void personResult(String number, PersonResult p) {
        Notification.Builder b = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(number)
                .setContentText(p.getName())
                .setTicker(String.format("%s: %s", number, p.getName()));
        notificationManager.notify(0, b.build());
    }

    public void companyResult(String number, CompanyResult c) {
        Notification.Builder b = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(number)
                .setContentText(c.getName())
                .setTicker(String.format("%s: %s", number, c.getName()));
        notificationManager.notify(0, b.build());
    }

    public void noResult(String number) {
        Notification.Builder b = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(number)
                .setContentText("Number not found")
                .setTicker(String.format("%s: not found", number));
        notificationManager.notify(0, b.build());
    }

    public void searchError(String number) {
        Notification.Builder b = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(number)
                .setContentText("Search error")
                .setTicker(String.format("%s: search error", number));
        notificationManager.notify(0, b.build());
    }

    public boolean contactExists(String phoneNumber) {
        ContentResolver localContentResolver = getContentResolver();
        Cursor contactLookupCursor = localContentResolver.query(
                Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber)),
                new String[] {PhoneLookup.DISPLAY_NAME},
                null,
                null,
                null);

        try {
            return (contactLookupCursor.getCount() > 0);
        }
        finally {
            contactLookupCursor.close();
        }
    }
}
